"use strict";

/*


colors.on_change = (r, g, b) => {
    colors.upload(r, g, b);
    draw();
};


colors.upload(1, 0, 0);


*/

const query = parameters();
const initial_preset = query['preset'] || 'sierpinski';
const initial_iteration = query['iteration'] || 2;

// gl context and view
const line_shader = new Shader('#line-vertex', '#line-fragment');
const face_shader = new Shader('#face-vertex', '#face-fragment');
const grid_shader = new Shader('#grid-vertex', '#grid-fragment');
const colors = new ColorUniform([line_shader, face_shader]);
const view = new View([line_shader, face_shader, grid_shader], gl.canvas);
const world = new World([line_shader, face_shader]);

// meshes
const lines = new Mesh(line_shader, gl.LINES, {
    attributes: [{ count: 3 }, { count: 1 }]
});
const faces = new Mesh(face_shader, gl.TRIANGLE_FAN, {
    index: true,
    attributes: [
        { count: 3 },
        { count: 1},
        { count: 3, normalize: true }
    ]
});
const grid = create_grid(grid_shader, '#grid-length', '#grid-space', draw);

// logic
const iteration = new Iteration('#iteration', initial_iteration);
const actions = new Actions('#actions');
const editor = new Editor('#editor-wrapper');
const system = new System();
const turtle = new Turtle();

// ui
const preview = document.querySelector('#preview');
const presets = new Presets('#presets', '#preset-definitions', initial_preset);




function update_logic() {
    if (actions.update()) {
        turtle.load_actions(actions.actions);
    }

    if (presets.update()) {
        editor.load(presets.source);
    }

    if (editor.update()) {
        system.load(editor.code)
    }

    const changed = system.update();
    const iteration_changed = iteration.update();
    if (changed.system || iteration_changed) {
        turtle.load_state(system.iteration(iteration.value), system.angle);
        preview.value = system.iteration(iteration.value);
        M.textareaAutoResize(preview)
    }

    if (turtle.update()) {
        lines.load(turtle.lines);
        faces.load(turtle.faces, turtle.indices);
    }

    if (changed.world) {
        world.load(system.world);
    }
    if (iteration_changed) {
        world.load_iteration(iteration.value);
    }

    if (changed.color) {
        colors.load(system.colors)
    }
}


function update_view(delta) {
    const [width, height] = gl.correct_size();
    view.update(width , height, delta);
    colors.update();
    world.update();
}


function draw() {
    gl.start_frame();
    grid.draw(100);
    gl.clear(gl.DEPTH_BUFFER_BIT);
    lines.draw();
    faces.draw();
}

let start_time = 0;
const overlay = document.querySelector('#overlay');
function render_loop(now) {
    const delta = now - start_time;
    overlay.innerText = delta.toFixed(1);
    start_time = now;
    update_logic();
    update_view(delta / 1000);
    draw();
    window.requestAnimationFrame(render_loop)
}

window.requestAnimationFrame(render_loop);