class Transform {
    constructor(x=0, y=0, z=0) {
        this.reset(x, y, z);
    }

    reset(x=0, y=0, z=0) {
        this.position = glm.vec3(x, y, z);
        this.forward = glm.vec3(0, 0, 1);
        this.left = glm.vec3(-1, 0, 0);
        this.up = glm.vec3(0, 1, 0);
    }

    copy() {
        const c = new Transform();
        c.position.copy(this.position);
        c.forward.copy(this.forward);
        c.left.copy(this.left);
        c.up.copy(this.up);
        return c;
    }

    yaw(angle) {
        const q = glm.angleAxis(angle, this.up);
        this.forward = glm.normalize(q.mul(this.forward));
        this.left = glm.normalize(q.mul(this.left));
    }

    roll(angle) {
        const q = glm.angleAxis(-angle, this.forward);
        this.left = glm.normalize(q.mul(this.left));
        this.up = glm.normalize(q.mul(this.up));
    }

    pitch(angle) {
        const q = glm.angleAxis(angle, this.left);
        this.forward = glm.normalize(q.mul(this.forward));
        this.up = glm.normalize(q.mul(this.up));
    }

    move_left(distance) {
        this.position.add_eq(this.left.mul(distance));
    }

    move_forward(distance) {
        this.position.add_eq(this.forward.mul(distance));
    }

    move_up(distance) {
        this.position.add_eq(this.up.mul(distance));
    }
}