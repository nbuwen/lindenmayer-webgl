"use strict";


class ColorUniform {
    constructor(shaders) {
        this.uniform = new Uniform(shaders, 'colors', 'vec3v');
        this.colors = [];
        this.max_colors = 10;
        this.fill_missing();
        this.changed = true;
    }

    update() {
        if (!this.changed) {
            return;
        }
        this.changed = false;
        this.uniform.upload(this.colors);
    }

    fill_missing() {
        while (this.colors.length < (this.max_colors * 3)) {
            this.colors.push(0, 0, 0);
        }
    }

    load(colors) {
        this.colors = [];
        colors.forEach(color => {
            const [r, g, b] = ColorUniform.hex_to_rgb(color);
            this.colors.push(r, g, b)
        });
        this.fill_missing();
        this.changed = true;
    }

    static hex_to_rgb(hex) {
        const r = hex.substr(1, 2);
        const g = hex.substr(3, 2);
        const b = hex.substr(5, 2);

        return [r, g, b].map(h => parseInt(h, 16) / 255)
    }
}
