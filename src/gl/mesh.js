"use strict";


class Mesh {
    constructor(shader, mode, options) {
        this.shader = shader;

        this.vbo = Mesh.create_vbo();
        if (options.index) {
            this.ibo = Mesh.create_ibo();
        }
        this.vao = this.create_vao(options.attributes);
        this.elements = 0;
        this.mode = mode;

        gl.bindBuffer(gl.ARRAY_BUFFER, null);
    }

    load(vertices, indices) {
        this.elements = vertices.length / this.offset;
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        if (indices) {
            this.elements = indices.length;
            gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.ibo);
            gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indices), gl.STATIC_DRAW);
            gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
        }
    }

    draw(n=1) {
        if (n === 0 || this.elements === 0) {
            return;
        }

        this.shader.use();
        gl.bindVertexArray(this.vao);
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo);

        if (this.ibo) {
            gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.ibo);
            gl.drawElements(this.mode, this.elements, gl.UNSIGNED_SHORT, 0);
            gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
        } else {
            if (n === 1) {
                gl.drawArrays(this.mode, 0, this.elements);
            } else {
                gl.drawArraysInstanced(this.mode, 0, this.elements, n);
            }
        }

        gl.bindBuffer(gl.ARRAY_BUFFER, null);
        gl.bindVertexArray(null);
    }

    create_vao(attributes) {
        const vao = gl.createVertexArray();
        gl.bindVertexArray(vao);

        this.offset = 0;
        const stride = attributes.map(i => i.count).reduce((i, j) => i+j) * 4;

        attributes.forEach((attribute, i) => {

            gl.enableVertexAttribArray(i);
            gl.vertexAttribPointer(i, attribute.count, gl.FLOAT, !!attribute.normalize, stride, this.offset * 4);
            this.offset += attribute.count
        });

        gl.bindVertexArray(null);

        return vao
    }

    static create_ibo() {
        const ibo = gl.createBuffer();
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, ibo);
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array([]), gl.STATIC_DRAW);

        return ibo;
    }

    static create_vbo() {
        const vbo = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, vbo);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([]), gl.STATIC_DRAW);

        return vbo;
    }
}


function create_grid(shader, length, space, on_change) {
    shader.connect_input(length, 'length');
    shader.connect_input(space, 'space');

    document.querySelectorAll(length + ',' + space).forEach(e => {
        e.addEventListener('input', on_change)
    });

    const grid = new Mesh(shader, gl.LINES, {attributes: [{ count: 2 }]});
    const vertices = [-1, 1, 1, 1, -1, -1, 1, -1, 1, -1, 1, 1, -1, -1, -1, 1];
    grid.load(vertices);
    return grid;
}