class View {
    constructor(shaders, input) {
        this.projection = glm.mat4();
        this.uniform = new Uniform(shaders, 'projection', 'mat4');
        this.aspect = 1;
        this.projection_changed = true;
        this.is_perspective = true;

        this.view = glm.mat4();
        this.global_up = glm.vec3(0, 1, 0);
        this.view_changed = true;
        this.velocity = glm.vec3(0, 0, 0);
        this.reset();

        this.input = input;

        const pointer_lock_listener = () => this.handle_pointer_lock();
        document.addEventListener('pointerlockchange', pointer_lock_listener);
        document.addEventListener('mozpointerlockchange', pointer_lock_listener);

        this.mouse_move_listener = event => this.handle_mouse_move(event);

        this.input.addEventListener('mousedown', event => {
            if (event.button === 0) {
                this.input.requestPointerLock()
            }
        });
        this.input.addEventListener('keydown', event => this.handle_key_press(event, true));
        this.input.addEventListener('keyup', event => this.handle_key_press(event, false));
        this.input.addEventListener('wheel', (event) => this.handle_mouse_wheel(event));
    }

    handle_pointer_lock() {
        if (this.input === document.pointerLockElement || this.input === document.mozPointerLockElement) {
            this.input.addEventListener('mousemove', this.mouse_move_listener);
        } else {
            this.input.removeEventListener('mousemove', this.mouse_move_listener);
        }
    }

    reset() {
        this.look_at = glm.vec3(0, 0, 0);
        this.model_to_camera = glm.vec3(0, -1, 1);
        this.left = glm.vec3(-1, 0, 0);
        this.up = glm.vec3(0, 1, 0);
    }

    handle_mouse_wheel(event) {
        if (document.pointerLockElement !== null) {
            event.preventDefault();
            this.model_to_camera.mul_eq(Math.pow(1.1, event.deltaY));
            this.view_changed = true;
            if (!this.is_perspective) {
                this.projection_changed = true;
            }
        }
    }

    handle_key_press(event, down) {
        const speed = down? 1 : 0;

        switch (event.key) {
            case 'r':
                this.reset();
                this.view_changed = true;
                break;
            case 'w':
                this.velocity.z = speed;
                break;
            case 's':
                this.velocity.z = -speed;
                break;
            case 'a':
                this.velocity.x = -speed;
                break;
            case 'd':
                this.velocity.x = speed;
                break;
            case 'q':
                this.velocity.y = speed;
                break;
            case 'e':
                this.velocity.y = -speed;
                break;
            case 'o':
            case 'p':
                if (down) {
                    this.is_perspective = !this.is_perspective;
                }
                break;
        }
    }

    handle_mouse_move(event) {
        const x = event.movementX;
        const y = event.movementY;

        if (x !== 0) {
            const q = glm.angleAxis(-x * glm.pi / 180, this.global_up);
            this.model_to_camera = q.mul(this.model_to_camera);
            this.left = q.mul(this.left);
            this.up = q.mul(this.up);
            this.view_changed = true;
        }
        if (y !== 0) {
            const q = glm.angleAxis(-y * glm.pi / 180, this.left);
            this.model_to_camera = q.mul(this.model_to_camera);
            this.up = q.mul(this.up);
            this.view_changed = true;
        }
    }

    update(width, height, delta) {
        const aspect = width / height;
        if (aspect !== this.aspect) {
            this.projection_changed = true;
            this.aspect = aspect;
        }

        if (this.projection_changed) {
            if (this.is_perspective) {
                this.projection = glm.perspective(90 * glm.pi / 180, this.aspect, 0.01, 1000);
            } else {
                const scale = glm.length(this.model_to_camera);
                this.projection = glm.ortho(-scale*aspect, scale*aspect, -scale, scale, 0.01, 1000);
            }
        }
        if (this.velocity.x || this.velocity.y || this.velocity.z) {
            const direction = glm.vec3(0, this.velocity.y, 0);
            direction.add_eq(this.left.mul(this.velocity.x));
            const forward = glm.cross(this.global_up, this.left);
            direction.add_eq(forward.mul(this.velocity.z));

            const speed = glm.max(glm.length(this.look_at), 1);
            this.look_at.add_eq(direction.mul(delta * speed));
            this.view_changed = true;
        }
        if (this.view_changed) {
            this.view = glm.lookAt(this.look_at.sub(this.model_to_camera), this.look_at, this.up)
        }

        if (this.view_changed || this.projection_changed) {
            this.view_changed = false;
            const matrix = this.projection.mul(this.view);
            this.uniform.upload(matrix)
        }

    }
}