"use strict";


class Uniform {
    constructor(shaders, name, type) {
        this.shaders = shaders.map(shader => ({
            shader: shader,
            location: shader.location_of(name)
        }));

        switch (type) {
            case 'vec3v':
                this.f = (v, l) => gl.uniform3fv(l, v);
                break;
            case 'mat3':
                this.f = (v, l) => gl.uniformMatrix3fv(l, false, v.elements);
                break;
            case 'mat4':
                this.f = (v, l) => gl.uniformMatrix4fv(l, false, v.elements);
                break;
            default:
                throw "UNKNOWN UNIFORM TYPE " + type
        }
    }

    upload(value) {
        this.shaders.forEach(shader => {
            shader.shader.use();
            this.f(value, shader.location)
        })
    }
}


class Shader {
    constructor(vertex_selector, fragment_selector) {
        const vertex_shader = Shader.create_shader(vertex_selector, gl.VERTEX_SHADER);
        const fragment_shader = Shader.create_shader(fragment_selector, gl.FRAGMENT_SHADER);

        this.program = Shader.create_program(vertex_shader, fragment_shader);

        gl.deleteShader(vertex_shader);
        gl.deleteShader(fragment_shader);
    }

    location_of(name) {
        this.use();
        return gl.getUniformLocation(this.program, name);
    }

    upload_matrix(location, matrix, dim=4) {
        this.use();
        if (dim === 4) {
            gl.uniformMatrix4fv(location, false, matrix.elements);
        } else {
            gl.uniformMatrix3fv(location, false, matrix.elements);
        }
    }

    connect_input(selector, uniform) {
        const location = gl.getUniformLocation(this.program, uniform);
        const input = document.querySelector(selector);
        input.addEventListener('input', () => {
            this.use();
            gl.uniform1f(location, input.value);
        });
        this.use();
        gl.uniform1f(location, input.value);
    }

    use() {
        gl.useProgram(this.program)
    }

    static create_shader(selector, type) {
        const source = document.querySelector(selector).text.trim();
        const shader = gl.createShader(type);
        gl.shaderSource(shader, source);
        gl.compileShader(shader);

        if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
            error.show("Shader Error", gl.getShaderInfoLog(shader));
            gl.deleteShader(shader);
        }

        return shader
    }

    static create_program(vertex_shader, fragment_shader) {
        const program = gl.createProgram();
        gl.attachShader(program, vertex_shader);
        gl.attachShader(program, fragment_shader);
        gl.linkProgram(program);

        if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
            error.show("Program Error", gl.getProgramInfoLog(program));
            gl.deleteProgram(program)
        }

        return program;
    }
}