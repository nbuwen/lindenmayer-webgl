"use strict";

const canvas = document.querySelector('#canvas');
const gl = canvas.getContext('webgl2');

if (!gl) {
    error.show("General Error", "Your browser does not support WebGL");
}

gl.clearColor(0.2, 0.2, 0.2, 1.0);
gl.clearDepth(1.0);
gl.clear(gl.COLOR_BUFFER_BIT);
gl.enable(gl.DEPTH_TEST);

gl.start_frame = function() {
    this.clear(this.COLOR_BUFFER_BIT | this.DEPTH_BUFFER_BIT);
};

gl.width = () => canvas.clientWidth;
gl.height = () => canvas.clientHeight;

gl.correct_size = function() {
    const width = canvas.clientWidth;
    const height = canvas.clientHeight;

    canvas.width = width;
    canvas.height = height;

    gl.viewport(0, 0, width, height);
    return [width, height];
};

window.addEventListener('resize', function() {
    gl.correct_size();
});