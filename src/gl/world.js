class World {
    constructor(shaders) {
        this.uniforms = {
            model: new Uniform(shaders, 'model_matrix', 'mat4'),
            normal: new Uniform(shaders, 'normal_matrix', 'mat3')
        };

        this.iteration = 1;
        this.changed = false;
    }

    load(world) {
        this.model = world.model;
        this.step = world.step;
        this.changed = true;
    }

    load_iteration(iteration) {
        this.iteration = iteration;
        this.changed = true;
    }

    static create_matrix(description) {
        const matrix = glm.mat4();

        const scale = description.scale;
        matrix.mul_eq(glm.scale(glm.vec3(scale, scale, scale)));
        matrix.mul_eq(glm.rotate(description.yaw * glm.pi / 180, glm.vec3(0, 1, 0)));
        matrix.mul_eq(glm.rotate(description.pitch * glm.pi / 180, glm.vec3(-1, 0, 0)));

        return matrix;
    }

    update() {
        if (!this.changed) {
            return
        }

        let model_matrix = World.create_matrix(this.model);
        let total_matrix = glm.mat4();
        if (this.iteration > 0) {
            const step_matrix = World.create_matrix(this.step);
            for (let i = 0; i < this.iteration; ++i) {
                total_matrix.mul_eq(step_matrix)
            }

            total_matrix = glm.inverse(total_matrix);
        }

        total_matrix.mul_eq(model_matrix);
        const normal_matrix = glm.mat3(glm.transpose(glm.inverse(total_matrix)));

        this.uniforms.model.upload(total_matrix);
        this.uniforms.normal.upload(normal_matrix);

        this.changed = false;
    }
}