class Presets {
    constructor(input, definitions, initial) {
        this.select = document.querySelector(input);
        const data = document.querySelectorAll(definitions + ' > div');
        this.map = {};
        this.selection = '';

        data.forEach(div => {
            const group = document.createElement('optgroup');
            group.label = div.getAttribute('data-title');
            this.select.appendChild(group);

            div.querySelectorAll('div').forEach(div => {
                const option = document.createElement('option');
                option.value = div.className;
                option.innerText = div.getAttribute('data-title');
                group.appendChild(option);
                this.map[div.className] = div;
            })
        });
        this.select.value = initial;
        materialize_select(input);
    }

    get source() {
        return this.map[this.selection].innerText.trim();
    }

    update() {
        if (this.selection !== this.select.value) {
            this.selection = this.select.value;
            return true;
        }
        return false;
    }

}
