function create_action_select(value) {
    const select = instantiate('actions');
    select.value = value;

    return select
}

function add_default_action(actions, select, symbols, key) {
    const row = create_row(actions);
    const left = create_column(6, 12, row);
    const right = create_column(6, 12, row);

    create_input('text', symbols.trim(), left, 'pattern');
    right.appendChild(create_action_select(key))
}

function create_action_ui() {
    const select = create_action_select();
    const actions = document.querySelector('#actions');

    add_default_action(actions, select, 'ABCD FG LR X', 'Draw');
    add_default_action(actions, select, 'f x', 'Skip');
    add_default_action(actions, select, '-', 'TurnLeft');
    add_default_action(actions, select, '+', 'TurnRight');
    add_default_action(actions, select, '^', 'PitchUp');
    add_default_action(actions, select, '&', 'PitchDown');
    add_default_action(actions, select, '\\', 'RollLeft');
    add_default_action(actions, select, '/', 'RollRight');
    add_default_action(actions, select, '[', 'Push');
    add_default_action(actions, select, ']', 'Pop');
    add_default_action(actions, select, '|', 'TurnAround');
    add_default_action(actions, select, '{', 'BeginFace');
    add_default_action(actions, select, '}', 'EndFace');
    add_default_action(actions, select, "'", 'NextColor');


}

create_action_ui();
materialize_select('select');