"use strict";


function _add_to_parent(element, parent) {
    if (parent !== null) {
        parent.appendChild(element)
    }
}

function create_row(parent=null) {
    const div = document.createElement('div');
    div.classList.add('row');

    _add_to_parent(div, parent);

    return div;
}

function create_column(medium, small, row=null) {
    const div = document.createElement('div');
    div.classList.add('col', 'm' + medium, 's' + small);

    _add_to_parent(div, row);

    return div;
}

function create_input(type, value='', parent=null, cls='') {
    const input = document.createElement('input');
    input.type = type;
    input.value = value;
    input.className = cls;

    _add_to_parent(input, parent);

    return input;
}

function instantiate(key) {
    const template = document.querySelector('template.' + key).content;
    const fragment = document.importNode(template, true);
    return fragment.firstElementChild;
}