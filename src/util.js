function parameters() {
    const query = location.search.substr(1);
    const result = {};
    query.split('&').map(parameter => {
        const [key, value] = parameter.split('=').map(decodeURIComponent);
        result[key] = value;
    });
    return result;
}