const error = {
    log: document.querySelector('#error-log'),

    show: function(caption, message) {
        this.log.visible = true;

        const heading = document.createElement('h2');
        heading.innerText = caption;
        const error = document.createElement('pre');
        error.innerHTML = message;

        this.log.appendChild(heading);
        this.log.appendChild(error);

        console.error(message)

    }
};