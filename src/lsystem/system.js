"use strict";


function array_eq(left, right) {
    if (left.length !== right.length) {
        return false;
    }
    for (let i = 0; i < left.length; ++i) {
        if (left[i] !== right[i]) {
            return false;
        }
    }
    return true;
}


function obj_eq(left, right) {
    const sorted_keys = Object.keys(left).sort();

    if (!array_eq(sorted_keys, Object.keys(right).sort())) {
        return false;
    }

    for (let key of sorted_keys) {
        if (left[key] !== right[key]) {
            return false;
        }
    }
    return true;
}


class System {
    constructor() {
        this.axiom = '';
        this.angle = 0;
        this.model = {};
        this.per_step = {};
        this.rules = {};
        this.colors = [];
        this.states = [this.axiom];

        this.system_changed = false;
        this.world_changed = false;
        this.color_changed = false;
    }

    step() {
        const current = this.states[this.states.length - 1];
        const next = [];

        for (let symbol of current) {
            next.push(this.rules[symbol] || symbol)
        }

        this.states.push(next.join(''));
    }

    iteration(n) {
        n = parseInt(n);
        while (this.states.length < (n + 1)) {
            this.step();
        }

        return this.states[n]
    }

    get world() {
        return {
            model: this.model,
            step: this.per_step,
        }
    }

    load(code) {
        const {axiom, angle, model, step, rules, colors} = code;

        this.system_changed = axiom !== this.axiom || angle !== this.angle || !obj_eq(this.rules, rules);
        this.world_changed = !obj_eq(this.model, model) ||!obj_eq(this.per_step, step) ;
        this.color_changed = !array_eq(colors, this.colors);

        this.axiom = axiom;
        this.angle = angle;
        this.model = model;
        this.per_step = step;
        this.rules = rules;
        this.colors = colors;

        if (this.system_changed) {
            this.states = [this.axiom]
        }
    }

    update() {
        const changed = {
            system: this.system_changed,
            world: this.world_changed,
            color: this.color_changed
        };

        this.system_changed = false;
        this.world_changed = false;
        this.color_changed = false;

        return changed;
    }
}