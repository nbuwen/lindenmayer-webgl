class Iteration {
    constructor(selector, initial) {
        this.range = document.querySelector(selector);
        this.range.value = initial;
        this.changed = true;

        this.range.addEventListener('change', () => this.changed = true);
    }

    get value() {
        return parseInt(this.range.value)
    }

    update() {
        if (!this.changed) {
            return false;
        }
        this.changed = false;

        return true;
    }
}