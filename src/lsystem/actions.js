"use strict";


class Actions {
    constructor(selector) {
        this.keys = document.querySelectorAll(selector + ' input.pattern');
        this.values = document.querySelectorAll(selector + ' select.action');
        this.actions = {};
        this.changed = true;

        this.keys.forEach(key => {
            key.addEventListener('change', () => this.changed = true)
        });
        this.values.forEach(value => {
            value.addEventListener('change', () => this.changed = true)
        })
    }

    update() {
        if (!this.changed) {
            return false
        }

        this.changed = false;

        this.actions = {};
        for (let i in this.keys) {
            const key = this.keys[i];
            const value = this.values[i];

            if (!key.value) {
                continue;
            }

            key.value.split('').filter(c => c !== ' ').forEach(c => {
                this.actions[c] = value.value;
            })
        }

        return true
    }
}