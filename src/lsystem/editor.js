"use strict";


function comment(line) {
    return `<span class="comment">${line}</span>`;
}

function keyword(cmd, space, args, namespace='') {
    return `<span class="keyword ${cmd} ${namespace}">${cmd}</span>${space}<span class="value ${cmd} ${namespace}">${args}</span>`;
}

function unique(cmd, space, args, found, namespace='') {
    if (found[namespace+cmd]) {
        return `<s class="duplicate">${cmd}${space}${args}</s>`
    }
    found[namespace+cmd] = true;
    return keyword(cmd, space, args, namespace);
}

function float(cmd, space, args, found, namespace='') {
    if (!args.match(/^[+-]?\d+\.?\d*$/)) {
        return null;
    }
    return unique(cmd, space, args, found, namespace);
}

function color(cmd, space, args) {
    if (!args.match(/^#[a-fA-F0-9]{6}$/)) {
        return null;
    }
    return keyword(cmd, space, args) + `<span class="color-box" style="background-color: ${args}"></span>`;
}


class Editor {
    constructor(selector) {
        this.wrapper = document.querySelector(selector);
        this.text = this.wrapper.querySelector('.editor');
        this.display = this.wrapper.querySelector('.display');
        this.color_picker = document.createElement('input');
        this.color_picker.type = 'color';
        this.text.contentEditable = true;
        this.display.contentEditable = true;
        this.text.addEventListener('input', () => {
            this.changed = true;
        });

        this.changed = true;
    }

    load(source) {
        this.text.innerText = source;
        this.changed = true;
    }

    string(command, default_value) {
        const value = this.display.querySelector('.value.' + command);
        return value? value.innerText : default_value;
    }

    float(command, default_value) {
        return parseFloat(this.string(command, default_value));
    }

    get code() {
        const rules = {};
        this.display.querySelectorAll('.value.Rule').forEach(rule => {
            rules[rule.classList[1]] = rule.innerText.trim();
        });
        const colors = [];
        this.display.querySelectorAll('.value.Color').forEach(color => {
            colors.push(color.innerText);
        });

        return {
            axiom: this.string('Axiom', ''),
            angle: this.float('Angle', 0),

            model: {
                yaw: this.float('Model.Yaw', 0),
                pitch: this.float('Model.Pitch', 0),
                roll: this.float('Model.Roll', 0),
                scale: this.float('Model.Scale', 1),
            },
            step: {
                yaw: this.float('Step.Yaw', 0),
                pitch: this.float('Step.Pitch', 0),
                roll: this.float('Step.Roll', 0),
                scale: this.float('Step.Scale', 1),
            },

            rules: rules,
            colors: colors
        }
    }

    update() {
        if (!this.changed) {
            return false;
        }

        let found = {};
        let namespace = '';

        this.display.innerHTML = this.text.innerText.split('\n').map(line => {
            const match = line.match(/(\S+)(?:(\s+)(.*))?/);
            if (!match) {
                return line;
            }
            const [, cmd, space='', args=''] = match;
            switch (cmd) {
                case '#':
                    return comment(line);
                case 'Angle':
                    return float(cmd, space, args, found) || line;
                case 'Scale':
                case 'Pitch':
                case 'Yaw':
                case 'Roll':
                    return float(cmd, space, args, found, namespace) || line;
                case 'Color':
                    return color(cmd, space, args) || line;
                case 'Axiom':
                    return unique(cmd, space, args, found);
                case 'Model:':
                case 'Step:':
                    if (space || args) {
                        return line;
                    }
                    namespace = cmd.substr(0, cmd.length - 1);
                    return unique(cmd, '', '', found, namespace);
                default:
                    if (cmd.length !== 1 || !space ||!args) {
                        return line;
                    }
                    return unique(cmd, space, args, found, namespace="Rule");
            }
        }).join('<br>');

        this.changed = false;
        return true;
    }
}
