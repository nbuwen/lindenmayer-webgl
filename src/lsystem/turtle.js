"use strict";


class Turtle {
    constructor() {
        this.reset();
        this.actions = {};
        this.angle = 0;
        this.state = '';

        this.changed = false;
    }

    load_actions(actions) {
        this.actions = actions;
        this.changed = true;
    }

    load_state(state, angle) {
        this.angle = angle / 180 * glm.pi;
        this.state = state;
        this.changed = true;
    }

    add_vertex() {
        const p = this.transform.position;
        this.lines.push(p.x, p.y, p.z, this.color);
    }

    add_face_vertex() {
        const p = this.transform.position;
        this.faces.push(p.x, p.y, p.z, this.color, 0, 0, 0);
    }

    start_line() {
        if (this.inside_face) {
            this.indices.push(this.faces.length / 7);
            this.add_face_vertex()
        } else  if (!this.line_pending) {
            this.line_pending = true;
            this.add_vertex();
        }
    }

    end_line() {
        if (this.inside_face) {

        } if (this.line_pending) {
            this.line_pending = false;
            this.add_vertex();
        }
    }

    reset() {
        this.transform = new Transform();

        this.inside_face = false;
        this.lines = [];
        this.faces = [];
        this.indices = [];
        this.color = 0;

        this.stack = [];
    }

    *run_length_encoded_actions() {
        if (this.state !== '') {
            let last = this.actions[this.state[0]] || 'Nothing';
            let count = 1;

            for (let i = 1; i < this.state.length; ++i) {
                const action = this.actions[this.state[i]] || 'Nothing';

                if (action === last) {
                    count += 1;
                } else {
                    if (last !== 'Nothing') {
                        yield [count, last];
                    }
                    last = action;
                    count = 1
                }
            }
            if (last !== 'Nothing') {
                yield [count, last]
            }
        }
    }

    update() {
        if (!this.changed) {
            return false
        }

        this.reset();

        for (let [count, action] of this.run_length_encoded_actions()) {
            const func = Action[action];
            if (func) {
                func(this, count);
            }
        }
        this.end_line();
        this.generate_normals();
        this.changed = false;

        const box = [Infinity, -Infinity, Infinity, -Infinity, Infinity, -Infinity];
        for (let i = 0; i < this.lines.length; i = i + 4) {
            const [x, y, z] = [this.lines[i], this.lines[i+1], this.lines[i+2]];

            box[0] = Math.min(box[0], x);
            box[1] = Math.max(box[1], x);
            box[2] = Math.min(box[2], y);
            box[3] = Math.max(box[3], y);
            box[4] = Math.min(box[4], z);
            box[5] = Math.max(box[5], z);


        }
        const [x, y, z] = [box[1] - box[0], box[3] - box[2], box[5] - box[4]];
        console.log("Bounding Box:", x, y, z);

        return true;
    }

    position_of(i) {
        const [x, y, z] = this.faces.slice(i * 7, i*7 + 3);
        return glm.vec3(x, y, z);
    }

    normal_for_triangle(a, b, c) {
        a = this.position_of(a);
        b = this.position_of(b);
        c = this.position_of(c);

        const ab = b.sub(a);
        const ac = c.sub(a);

        return glm.cross(ab, ac);
    }

    apply_normal(index, normal) {
        index *= 7;
        normal = glm.normalize(normal);
        this.faces[index + 4] = normal.x;
        this.faces[index + 5] = normal.y;
        this.faces[index + 6] = normal.z;
    }

    generate_normals_for_face(face) {
        if (face.length < 3) {
            return;
        }
        const [root, left, ...middle] = face;
        const right = middle.pop();

        const normals = [];
        for (let i = 2; i < face.length; ++i) {
            normals.push(this.normal_for_triangle(root, face[i-1], face[i]));
        }
        this.apply_normal(left, normals[0]);
        this.apply_normal(right, normals[normals.length - 1]);

        for (let i = 0; i < middle.length; ++i) {
            const left = normals[i];
            const right = normals[i+1];
            this.apply_normal(middle[i], left.add(right));
        }

        this.apply_normal(root, normals.reduce(glm.add));
    }

    generate_normals() {
        const face = [];
        for (let index of this.indices) {
            if (index === -1) {
                this.generate_normals_for_face(face);
                face.length = 0;
            } else {
                face.push(index);
            }
        }
    }

    static draw(turtle, count) {
        turtle.start_line();
        turtle.transform.move_forward(count);
    }

    static skip(turtle, count) {
        turtle.end_line();
        turtle.transform.move_forward(count)
    }

    turn(angle) {
        this.end_line();
        this.transform.yaw(angle)
    }

    static turn_left(turtle, count) {
        turtle.turn(turtle.angle * count)
    }

    static turn_right(turtle, count) {
        turtle.turn(-turtle.angle * count)
    }

    static turn_around(turtle, count) {
        turtle.turn(glm.pi * count);
    }

    pitch(angle) {
        this.end_line();
        this.transform.pitch(angle)
    }

    static pitch_up(turtle, count) {
        turtle.pitch(turtle.angle * count)
    }

    static pitch_down(turtle, count) {
        turtle.pitch(-turtle.angle * count)
    }

    roll(angle) {
        this.transform.roll(angle)
    }

    static roll_left(turtle, count) {
        turtle.roll(turtle.angle * count)
    }

    static roll_right(turtle, count) {
        turtle.roll(-turtle.angle * count)
    }

    static push(turtle, count) {
        turtle.end_line();
        for (let i = 0; i < count; ++i) {
            turtle.stack.push([turtle.transform.copy(), turtle.color])
        }
    }

    static pop(turtle, count) {
        turtle.end_line();
        for (let i = 0; i < count; ++i) {
            if (turtle.stack.length > 0) {
                [turtle.transform, turtle.color] = turtle.stack.pop();
            } else {
                turtle.transform = new Transform();
                turtle.color = 0
            }
        }
    }

    static begin_face(turtle) {
        turtle.end_line();
        turtle.inside_face = true;
    }

    static end_face(turtle) {

        turtle.indices.push(-1);
        turtle.inside_face = false;
    }

    static next_color(turtle, count) {
        turtle.color += count;
    }
}

const Action = Object.freeze({
    Nothing: null,
    Draw: Turtle.draw,
    Skip: Turtle.skip,

    TurnLeft: Turtle.turn_left,
    TurnRight: Turtle.turn_right,
    PitchUp: Turtle.pitch_up,
    PitchDown: Turtle.pitch_down,
    RollLeft: Turtle.roll_left,
    RollRight: Turtle.roll_right,
    TurnAround: Turtle.turn_around,

    Push: Turtle.push,
    Pop: Turtle.pop,
    BeginFace: Turtle.begin_face,
    EndFace: Turtle.end_face,
    NextColor: Turtle.next_color,
});